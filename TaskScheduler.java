class Solution {
    public int leastInterval(char[] tasks, int n) {
        int[] frequencies = new int[26];
        
        // Calculate the frequency of each task
        for (char task : tasks) {
            frequencies[task - 'A']++;
        }
        
        // Sort the frequencies in descending order
        Arrays.sort(frequencies);
        
        // Get the maximum frequency task
        int maxFreq = frequencies[25] - 1;
        
        // Calculate the idle slots
        int idleSlots = maxFreq * n;
        
        // Fill the idle slots with other tasks
        for (int i = 24; i >= 0 && frequencies[i] > 0; i--) {
            idleSlots -= Math.min(frequencies[i], maxFreq);
        }
        
        // If idleSlots is negative, return the total length of tasks
        return idleSlots > 0 ? idleSlots + tasks.length : tasks.length;
    }
}
