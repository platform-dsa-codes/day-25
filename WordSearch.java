class Solution {
    public boolean exist(char[][] board, String word) {
        if (board == null || board.length == 0 || word == null || word.length() == 0)
            return false;
        
        int m = board.length;
        int n = board[0].length;
        
        // Loop through each cell in the board
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // If the cell contains the first character of the word,
                // try to search for the word starting from this cell
                if (board[i][j] == word.charAt(0)) {
                    if (dfs(board, word, i, j, 0))
                        return true;
                }
            }
        }
        return false;
    }
    
    // Depth First Search (DFS) function to search for the word in the board
    private boolean dfs(char[][] board, String word, int i, int j, int index) {
        if (index == word.length())
            return true;
        
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length || board[i][j] != word.charAt(index))
            return false;
        
        // Mark the cell as visited by changing its value
        char temp = board[i][j];
        board[i][j] = '*';
        
        // Explore all four directions
        boolean found = dfs(board, word, i + 1, j, index + 1) ||
                        dfs(board, word, i - 1, j, index + 1) ||
                        dfs(board, word, i, j + 1, index + 1) ||
                        dfs(board, word, i, j - 1, index + 1);
        
        // Restore the cell's original value
        board[i][j] = temp;
        
        return found;
    }
}
